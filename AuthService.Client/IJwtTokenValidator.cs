﻿namespace AuthService.Client
{
    public interface IJwtTokenValidator
    {
        bool IsTokenValid(string token);
    }
}