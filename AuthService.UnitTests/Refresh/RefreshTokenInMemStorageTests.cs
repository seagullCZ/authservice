﻿using System;
using System.Timers;
using AuthService.Exceptions;
using AuthService.Refresh;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace AuthService.UnitTests.Refresh
{
    [TestFixture]
    public class RefreshTokenInMemStorageTests
    {
        private const string ValidTokenValue = "TokenToken";
        private const string ValidTokenValue2 = "TokToken";
        private const string ValidTokenValue3 = "AABBCCDD";
        private const string UnknownTokenValue = "UnknownToken12345";
        private const string ValidUserName = "john.doe";
        private const string ValidUserName2 = "jane.doe";
        private const string TokenNullValue = null;
        private const string NullUserLogin = null;
        private const string UnknownUserLogin = "unknown.user";

        private CurrentTestTimeProvider _currentTimeProviderMock;
        private Mock<ITimer> _storageCleanTimer;
        private Mock<ILogger<RefreshTokenInfoInMemStorage>> _loggerMock = new Mock<ILogger<RefreshTokenInfoInMemStorage>>();
        

        [SetUp]
        public void Setup()
        {
            _currentTimeProviderMock = new CurrentTestTimeProvider(DateTime.UtcNow);
            _storageCleanTimer = new Mock<ITimer>();
        }

        [Test]
        public void AddToken_AddingCorrectToken_TokenIsStoredAndReturned()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(42), DateTime.UtcNow);
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act
            storage.AddTokenInfo(validToken);
            var returnedTokenInfo = storage.GetTokenInfo(ValidTokenValue);

            // assert
            Assert.That(returnedTokenInfo, Is.Not.Null);
            Assert.That(returnedTokenInfo.Token, Is.EqualTo(ValidTokenValue));
        }
        
        [Test]
        public void AddToken_AddingMultipleTokensForSameUser_TokenIsStoredAndReturned()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(42), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(42), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);

            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(2));
        }

        [Test]
        public void AddToken_AddSameTokenTwice_ExceptionIsThrow()
        {
            // Arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(42), DateTime.UtcNow);
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            
            // act / assert
            Assert.Throws<TokenAddingFailedException>(() => storage.AddTokenInfo(validToken));
        }

        [Test]
        public void AddToken_AddNullToken_ThrowsException()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => storage.AddTokenInfo(null));
        }

        [Test]
        public void GetTokenInfo_AddTokenTokenExpired_ReturnsTokenNotFoundException()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(42), DateTime.UtcNow);
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            _currentTimeProviderMock.TestTime = DateTime.UtcNow + TimeSpan.FromDays(50);
            _storageCleanTimer.Raise(t => t.Elapsed += null, new EventArgs() as ElapsedEventArgs);
            
            // act / assert
            Assert.Catch<TokenNotFoundException>(() => storage.GetTokenInfo(ValidTokenValue));
        }
        
        [Test]
        public void GetTokenInfo_AddMultipleTokensTokenExpired_OnlyNonExpiredTokensRemains()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            
            // act
            _currentTimeProviderMock.TestTime = DateTime.UtcNow + TimeSpan.FromDays(15);
            _storageCleanTimer.Raise(t => t.Elapsed += null, new EventArgs() as ElapsedEventArgs);
            
            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(1));
        }
        
        [Test]
        public void RemoveToken_UnknownToken_ThrowsTokenNotFoundException()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(42), DateTime.UtcNow);
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            
            // act / assert
            Assert.Throws<TokenNotFoundException>(() => storage.GetTokenInfo(UnknownTokenValue));
            Assert.That(storage.TokensInStorage(), Is.EqualTo(1));
        }
        
        [Test]
        public void RemoveToken_RemoveValidToken_TokenIsNotPresentInStorage()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            
            // act 
            storage.RemoveToken(ValidTokenValue);
            
            // assert
            Assert.That(storage.ContainsToken(ValidTokenValue), Is.False);
            Assert.That(storage.ContainsToken(ValidTokenValue2), Is.True);
        }
        
        [Test]
        public void RemoveToken_StoredToken_TokenIsNotPresentInStorage()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.Throws<TokenNotFoundException>(() => storage.RemoveToken(UnknownTokenValue));
        }
        
        [Test]
        public void RemoveToken_TokenStringIsNull_ThrowsException()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentException>(() => storage.RemoveToken(TokenNullValue));
        }

        [Test]
        public void RevokeOwnerTokens_ValidOwnerUserName_UserTokensRemoved()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            var validToken3 = new RefreshTokenInfo(ValidUserName2, ValidTokenValue3, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            storage.AddTokenInfo(validToken3);
            
            // act
            storage.RemoveAllOwnerTokens(ValidUserName);
            
            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(1));
            Assert.That(storage.ContainsToken(ValidTokenValue3));
        }
        
        [Test]
        public void RevokeOwnerTokens_UnknownUser_AnyTokenIsRemoved()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            var validToken3 = new RefreshTokenInfo(ValidUserName2, ValidTokenValue3, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            storage.AddTokenInfo(validToken3);
            
            // act / assert
            Assert.DoesNotThrow(() => storage.RemoveAllOwnerTokens(UnknownUserLogin));
            
            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(3));
        }
        
        [Test]
        public void RevokeOwnerTokens_NullUser_ExceptionIsThrown()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentException>(() => storage.RemoveAllOwnerTokens(NullUserLogin));
        }

        [Test]
        public void RevokeAllTokens_NoTokenIsInStorage_NothingHappens()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.DoesNotThrow(() => storage.RemoveAllTokens());
            Assert.That(storage.TokensInStorage(), Is.EqualTo(0));
        }
        
        [Test]
        public void RevokeAllTokens_MultipleTokensInStorage_StorageIsEmpty()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            var validToken3 = new RefreshTokenInfo(ValidUserName2, ValidTokenValue3, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            storage.AddTokenInfo(validToken3);
            
            // act
            storage.RemoveAllTokens();

            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(0));
        }

        [Test]
        public void RemoveToken_MultipleTokensCorrectTokenValue_TokenIsRemoved()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            var validToken3 = new RefreshTokenInfo(ValidUserName2, ValidTokenValue3, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            storage.AddTokenInfo(validToken3);

            // act
            storage.RemoveToken(ValidTokenValue);
            
            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(2));
            Assert.That(storage.ContainsToken(ValidTokenValue2), Is.True);
            Assert.That(storage.ContainsToken(ValidTokenValue3), Is.True);
        }
        
        [Test]
        public void RemoveToken_MultipleTokensUnknownToken_ExceptionIsThrown()
        {
            // arrange
            var validToken = new RefreshTokenInfo(ValidUserName, ValidTokenValue, TimeSpan.FromDays(10), DateTime.UtcNow);
            var validToken2 = new RefreshTokenInfo(ValidUserName, ValidTokenValue2, TimeSpan.FromDays(20), DateTime.UtcNow);
            var validToken3 = new RefreshTokenInfo(ValidUserName2, ValidTokenValue3, TimeSpan.FromDays(20), DateTime.UtcNow);
            
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            storage.AddTokenInfo(validToken);
            storage.AddTokenInfo(validToken2);
            storage.AddTokenInfo(validToken3);

            // act 
            Assert.Throws<TokenNotFoundException>(() => storage.RemoveToken(UnknownTokenValue));
            
            // assert
            Assert.That(storage.TokensInStorage(), Is.EqualTo(3));
        }
        
        [Test]
        public void RemoveToken_TokenValueIsNull_ExceptionIsThrown()
        {
            // arrange
            var storage = new RefreshTokenInfoInMemStorageTestable(
                _currentTimeProviderMock, 
                _storageCleanTimer.Object, 
                _loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentException>(() => storage.RemoveToken(TokenNullValue));
        }
    }

    class RefreshTokenInfoInMemStorageTestable : RefreshTokenInfoInMemStorage
    {
        public RefreshTokenInfoInMemStorageTestable(ICurrentTimeProvider currentTimeProvider, ITimer storageCleanTimer, ILogger<RefreshTokenInfoInMemStorage> logger) 
            : base(currentTimeProvider, storageCleanTimer, logger)
        {
        }

        public bool ContainsToken(string token)
        {
            return _storage.TryGetValue(token, out var _);
        }

        public int TokensInStorage()
        {
            return _storage.Count;
        }
    }
}