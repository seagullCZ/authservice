﻿using System;
using AuthService.Refresh;
using NUnit.Framework;

namespace AuthService.UnitTests.Refresh
{
    [TestFixture]
    public class RefreshTokenInfoTests
    {
        private static readonly DateTime CurrentTime = DateTime.UtcNow;
        private static readonly TimeSpan TokenTimeSpan = TimeSpan.FromDays(42);
        private static readonly DateTime TimeBeforeExpiration = CurrentTime + TimeSpan.FromDays(32);
        private static readonly DateTime TimeAfterExpiration = CurrentTime + TimeSpan.FromDays(451);

        private RefreshTokenInfo _refreshTokenInfo;

        [SetUp]
        public void SetUp()
        {
            _refreshTokenInfo = new RefreshTokenInfo("john.doe", "1337", TokenTimeSpan, CurrentTime);
        }

        [Test]
        public void IsValid_CurrentTimeIsBeforeLifeSpanOfToken_TokenIsValid()
        {
            // act
            var isValid = _refreshTokenInfo.IsValid(TimeBeforeExpiration);
            
            // assert
            Assert.That(isValid, Is.True);
        }
        
        [Test]
        public void IsValid_CurrentTimeIsAfterLifeSpanOfToken_TokenIsInvalid()
        {
            // act
            var isValid = _refreshTokenInfo.IsValid(TimeAfterExpiration);
            
            // assert
            Assert.That(isValid, Is.False);
        }
    }
}