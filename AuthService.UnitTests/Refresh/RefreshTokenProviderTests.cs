﻿using System;
using AuthService.Crypto;
using AuthService.Exceptions;
using AuthService.Refresh;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace AuthService.UnitTests.Refresh
{
    [TestFixture]
    public class RefreshTokenProviderTests
    {
        private Mock<IRefreshTokenInfoStorage> refreshTokenStorageMock;
        private Mock<IRefreshTokenGenerator> refreshTokenGeneratorMock;
        private Mock<ILogger<RefreshTokenManager>> loggerMock;

        private const string TestToken = "TESTTOKEN";
        private const string TestUserName = "john.doe";
        private static readonly TimeSpan TestTokenValidTimeSpan = TimeSpan.FromDays(32);
        private static readonly DateTime TestTokenTimeStamp = DateTime.UtcNow;

        private static readonly DateTime OldDate = DateTime.UtcNow - TimeSpan.FromDays(42);

        [SetUp]
        public void SetUp()
        {
            refreshTokenStorageMock = new Mock<IRefreshTokenInfoStorage>();
            refreshTokenGeneratorMock = new Mock<IRefreshTokenGenerator>();
            loggerMock = new Mock<ILogger<RefreshTokenManager>>();
        }

        [Test]
        public void GetOwnerOfToken_EverythingOkTokenIsInCache_ReturnsToken()
        {
            // arrange
            refreshTokenStorageMock.Setup(r => r.GetTokenInfo(It.IsAny<string>()))
                .Returns<string>((token) =>
                    new RefreshTokenInfo(TestUserName, TestToken, TestTokenValidTimeSpan, TestTokenTimeStamp));
            
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act
            var tokenOwner = tokenProvider.GetOwnerOfToken(TestToken);
            
            // assert
            Assert.That(tokenOwner, Is.EqualTo(TestUserName));
            refreshTokenGeneratorMock.Verify(r => r.GenerateRefreshToken(), Times.Never);
            refreshTokenStorageMock.Verify(s => s.GetTokenInfo(It.Is<string>(token => token == TestToken)), Times.Once);
        }
        
        [Test]
        public void GetOwnerOfToken_ExpiredToken_ReturnsException()
        {
            // arrange
            refreshTokenStorageMock.Setup(r => r.GetTokenInfo(It.IsAny<string>()))
                .Returns<string>((token) =>
                    new RefreshTokenInfo(TestUserName, TestToken, TestTokenValidTimeSpan, OldDate));
            
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act / assert
            Assert.Throws<TokenNotFoundException>(() => tokenProvider.GetOwnerOfToken(TestToken));
            refreshTokenGeneratorMock.Verify(r => r.GenerateRefreshToken(), Times.Never);
            refreshTokenStorageMock.Verify(s => s.GetTokenInfo(It.Is<string>(token => token == TestToken)), Times.Once);
        }
        
        [Test]
        public void GetOwnerOfToken_TokenIsNull_ThrowsException()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => tokenProvider.GetOwnerOfToken(null));
            
            refreshTokenStorageMock.Verify(s => s.GetTokenInfo(It.IsAny<string>()), Times.Never);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }

        [Test]
        public void CreateTokenForOwner_EverythingIsOk_AddsTokenIntoCache()
        {
            // arrange
            refreshTokenGeneratorMock.Setup(g => g.GenerateRefreshToken()).Returns(TestToken);
            
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act
            var token = tokenProvider.CreateTokenForOwner(TestUserName);
            
            // assert
            Assert.That(token, Is.SameAs(TestToken));
            refreshTokenStorageMock.Verify(s => s.AddTokenInfo(It.IsAny<RefreshTokenInfo>()), Times.Once);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Once);
        }
        
        [Test]
        public void CreateTokenForOwner_UserNameIsNull_ThrowsException()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => tokenProvider.CreateTokenForOwner(null));
            
            refreshTokenStorageMock.Verify(s => s.AddTokenInfo(It.IsAny<RefreshTokenInfo>()), Times.Never);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }
        
        [Test]
        public void RevokeTokensOfOwner_EverythingOk_ReturnsToken()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act
            tokenProvider.RevokeTokensOfOwner(TestUserName);
            
            // assert
            refreshTokenStorageMock.Verify(s => s.RemoveAllOwnerTokens(It.Is<string>(owner => owner == TestUserName)), Times.Once);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }
        
        [Test]
        public void RevokeTokensOfOwner_OwnerNameIsNull_ThrowsException()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => tokenProvider.RevokeTokensOfOwner(null));
            
            refreshTokenStorageMock.Verify(s => s.RemoveAllOwnerTokens(It.IsAny<string>()), Times.Never);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }

        [Test]
        public void RevokeAllTokens_EverythingOk_RevokesAllTokensInStorage()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act
            tokenProvider.RevokeAllTokens();
            
            // assert
            refreshTokenStorageMock.Verify(s => s.RemoveAllTokens(), Times.Once);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }

        [Test]
        public void RevokeToken_CorrectToken_RevokeTokenInStorage()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act
            tokenProvider.RevokeToken(TestToken);
            
            // assert
            refreshTokenStorageMock.Verify(s => s.RemoveToken(It.Is<string>(token => token == TestToken)), Times.Once);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }

        [Test]
        public void RevokeToken_NullToken_ThrowsException()
        {
            // arrange
            var mockTime = DateTime.UtcNow;
            var timeProvider = new CurrentTestTimeProvider(mockTime);
            var tokenProvider = new RefreshTokenManager(
                refreshTokenStorageMock.Object, 
                refreshTokenGeneratorMock.Object, 
                timeProvider, 
                loggerMock.Object);
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => tokenProvider.RevokeToken(null));
            
            refreshTokenStorageMock.Verify(s => s.RemoveToken(It.IsAny<string>()), Times.Never);
            refreshTokenGeneratorMock.Verify(g => g.GenerateRefreshToken(), Times.Never);
        }
    }
}