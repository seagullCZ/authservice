﻿using System;

namespace AuthService.UnitTests
{
    public class CurrentTestTimeProvider : ICurrentTimeProvider
    {
        public DateTime TestTime { get; set; }

        public CurrentTestTimeProvider(DateTime testTime)
        {
            TestTime = testTime;
        }
        
        public DateTime GetCurrentTime()
        {
            return TestTime;
        }

        public DateTime GetCurrentUtcTime()
        {
            return TestTime.ToUniversalTime();
        }
    }
}