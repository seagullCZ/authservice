﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AuthService.Crypto;
using AuthService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using NUnit.Framework;

namespace AuthService.UnitTests.Crypto
{
    [TestFixture]
    public class JwtTokenGeneratorTests
    {
        private const string TokenHeader = @"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9";
        private const string TokenPayload = @"eyJuYW1lIjoiam9obi5kb2UiLCJyb2xlcyI6IkFkbWluLFNvbWV" +
                                            @"Sb2xlIiwiaXNFbmFibGVkIjoiVHJ1ZSIsImV4cCI6MTYyNzUwND" +
                                            @"YyNSwiaXNzIjoiVGhpc0FwcCIsImF1ZCI6IlRoaXNBcHAifQ";
        private const string TokenSignature = @"LIDzdeKUzYX_S3dgXDJ1QpvVnavBc_H3JM5VaknpMg0VkW8nq" +
                                              @"jsorERbLNCwZ7DGLXlHvuM4ARtJrQHdHGwM_g";
        private const int TokenHeaderIndex = 0;
        private const int TokenPayloadIndex = 1;
        private const int TokenSignatureIndex = 2;
        private const int JwtTokenPartsCount = 3;
        
        private static readonly Dictionary<string,string> CorrectStandardConfiguration = new()
        {
            {"Jwt:Key","633063306330633063306330"},
            {"Jwt:Issuer","ThisApp"},
            {"Jwt:TokenLifespanSeconds","120"}
        };
        
        private static readonly Dictionary<string,string> MissingIssuerConfiguration = new()
        {
            {"Jwt:Key","633063306330633063306330"},
            {"Jwt:TokenLifespanSeconds","120"}
        };

        private static readonly DateTime TestDateTime = new(2021, 07, 28, 22, 35, 5, 
            new CultureInfo("en-US", false).Calendar);
        
        [OneTimeSetUp]
        public void SetUp()
        {
            /* This enable potential PII (personally identifiable information) in exceptions. Without it,
             exceptions don't have enough information for debug. USE ONLY FOR DEBUG OR TESTS */
            IdentityModelEventSource.ShowPII = true;
        }
        
        [Test]
        public void GetJwtToken_UserIsNull_ExceptionIsThrown()
        {
            // arrange
            var tokenGenerator = new JwtTokenGenerator(
                new CurrentTestTimeProvider(TestDateTime), 
                CreateConfiguration(CorrectStandardConfiguration));
            
            // act / assert
            Assert.Catch<ArgumentNullException>(() => tokenGenerator.GenerateJwtToken(null));
        }
        
        [Test]
        public void GetJwtToken_UserDontHaveName_ExceptionIsThrown()
        {
            // arrange
            var user = new User
            {
                Login = String.Empty,
                IsEnabled = true
            };
            user.Roles = CreateUserRoles(new List<string>{"Admin","SomeRole"}, user);
            
            var tokenGenerator = new JwtTokenGenerator(
                new CurrentTestTimeProvider(TestDateTime), 
                CreateConfiguration(CorrectStandardConfiguration));
            
            // act / assert
            Assert.Catch<ArgumentException>(() => tokenGenerator.GenerateJwtToken(user));
        }
        
        [Test]
        public void GetJwtToken_UserDontHaveRoles_ExceptionIsThrown()
        {
            // arrange
            var user = new User
            {
                Login = String.Empty,
                IsEnabled = true
            };
            
            var tokenGenerator = new JwtTokenGenerator(
                new CurrentTestTimeProvider(TestDateTime), 
                CreateConfiguration(CorrectStandardConfiguration));
            
            // act / assert
            Assert.Catch<ArgumentException>(() => tokenGenerator.GenerateJwtToken(user));
        }
        
        [Test]
        public void GetJwtToken_MissingIssuerConfiguration_ExceptionIsThrown()
        {
            // act / assert
            Assert.Throws<ArgumentException>( ()=> new JwtTokenGenerator(
                new CurrentTestTimeProvider(TestDateTime), 
                CreateConfiguration(MissingIssuerConfiguration)));
        }
        
        [Test]
        public void GetJwtToken_EverythingCorrect_CorrectJwtTokenGenerated()
        {
            // arrange
            var user = new User
            {
                Login = "john.doe",
                IsEnabled = true
            };
            user.Roles = CreateUserRoles(new List<string>{"Admin","SomeRole"}, user);

            var tokenGenerator = new JwtTokenGenerator(
                new CurrentTestTimeProvider(TestDateTime), 
                CreateConfiguration(CorrectStandardConfiguration));
            
            // act
            var token = tokenGenerator.GenerateJwtToken(user);
            
            // assert
            Assert.That(token, Is.Not.Null);
            CheckToken(token, TokenHeader, TokenPayload, TokenSignature);
        }

        private static IConfiguration CreateConfiguration(IDictionary<string, string> configuration)
        {
            return new ConfigurationBuilder()
                .AddInMemoryCollection(configuration)
                .Build();
        }

        private static List<UserRole> CreateUserRoles(IEnumerable<string> roles, User user)
        {
            return roles.Select(r => new UserRole
            {
                User = user,
                Role = new Models.Role {Name = r}
            }).ToList();
        }

        private static void CheckToken(string token, string expectedHeader, string expectedPayload, string tokenSignature)
        {
            var tokenParts = token.Split(".");
            Assert.That(tokenParts.Length, Is.EqualTo(JwtTokenPartsCount), "Token should consist of 3 parts.");
            Assert.That(tokenParts[TokenHeaderIndex], Is.EqualTo(expectedHeader), "Token header do not corresponds with expected one.");
            Assert.That(tokenParts[TokenPayloadIndex], Is.EqualTo(expectedPayload), "Token payload do not corresponds with expected one.");
            Assert.That(tokenParts[TokenSignatureIndex], Is.EqualTo(tokenSignature), "Token signature do not corresponds with expected one.");
        }
    }
}