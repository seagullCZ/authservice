﻿using AuthService.Crypto;
using NUnit.Framework;

namespace AuthService.UnitTests.Crypto
{
    [TestFixture]
    public class RefreshTokenGeneratorTests
    {
        [Test]
        public void GenerateRefreshToken_GenerateTwoTokens_TokensAreNotSame()
        {
            // arrange
            var tokenGenerator = new RefreshTokenGenerator();
            
            // act
            var firstToken = tokenGenerator.GenerateRefreshToken();
            var secondToken = tokenGenerator.GenerateRefreshToken();
            
            // assert
            Assert.That(firstToken, Is.Not.SameAs(secondToken));
        }
        
        [Test]
        public void GenerateRefreshToken_TwoGeneratorsGenerateToken_TokensAreNotSame()
        {
            // arrange
            var firstTokenGenerator = new RefreshTokenGenerator();
            var secondTokenGenerator = new RefreshTokenGenerator();
            
            // act
            var firstToken = firstTokenGenerator.GenerateRefreshToken();
            var secondToken = secondTokenGenerator.GenerateRefreshToken();
            
            // assert
            Assert.That(firstToken, Is.Not.SameAs(secondToken));
        }
    }
}