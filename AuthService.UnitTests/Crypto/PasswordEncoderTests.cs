﻿using System;
using AuthService.Crypto;
using NUnit.Framework;

namespace AuthService.UnitTests.Crypto
{
    [TestFixture]
    public class PasswordEncoderTests
    {
        private const string CorrectPassword = "Pa55word11";
        private static readonly string EmptyPassword = String.Empty;
            
        [Test]
        public void EncodePassword_CorrectPassword_PasswordIsEncoded()
        {
            // arrange
            var passwordEncoder = new PasswordHashProvider();
            
            // act
            var passwordHash = passwordEncoder.HashPassword(CorrectPassword);
            
            // assert
            Assert.That(passwordHash, Is.Not.Null);
            Assert.That(passwordHash, Is.Not.EqualTo(CorrectPassword));
        }
        
        [Test]
        public void EncodePassword_PasswordIsEmpty_PasswordIsEncoded()
        {
            // arrange
            var passwordEncoder = new PasswordHashProvider();
            
            // act
            var passwordHash = passwordEncoder.HashPassword(EmptyPassword);
            
            // assert
            Assert.That(passwordHash, Is.Not.Null);
            Assert.That(passwordHash, Is.Not.EqualTo(EmptyPassword));
        }
        
        [Test]
        public void EncodePassword_PasswordIsNull_ExceptionIsThrown()
        {
            // arrange
            var passwordEncoder = new PasswordHashProvider();
            
            // act / assert
            Assert.Throws<ArgumentNullException>(() => passwordEncoder.HashPassword(null));
        }
    }
}