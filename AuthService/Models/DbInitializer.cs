﻿using System;
using System.Collections.Generic;

namespace AuthService.Models
{
    public class DbInitializer
    {
        public static void Initialize(AuthServiceDbContext context)
        {
            context.Database.EnsureCreated();

            var adminRole = new Role {Name = "Administrator"};
            context.Roles.Add(adminRole);

            var adminAccount = new User
            {
                Login = "Admin",
                Password = @"AGeEmgQohO5Y2exCuWIiA8HFRQTnNkdGm6TA/kwPa+cXapqKWJ95CsRwJhcuVsnWPg==",
                CreationTime = DateTime.UtcNow,
                LastUpdateTime = DateTime.UtcNow,
                IsEnabled = true
            };
            context.Users.Add(adminAccount);

            var adminUserRole = new UserRole
            {
                Role = adminRole,
                User = adminAccount
            };

            context.UserRoles.Add(adminUserRole);
            adminAccount.Roles = new List<UserRole> {adminUserRole};
            context.SaveChanges();
        }
    }
}