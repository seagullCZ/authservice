﻿using Microsoft.EntityFrameworkCore;

namespace AuthService.Models
{
    public class AuthServiceDbContext : DbContext
    {
        protected DbContextOptions<AuthServiceDbContext> ContextOptions { get; }
        
        public AuthServiceDbContext(DbContextOptions<AuthServiceDbContext> options) : base(options)
        {
            ContextOptions = options;
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasKey(ur => new { ur.UserId, ur.RoleId });
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
    }
}