﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace AuthService.Models
{
    [Index(nameof(Login), nameof(IsDeleted), IsUnique = true)]
    public class User
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Login { get; set; }
        
        [Required]
        public string Password { get; set; }
        
        [Required]
        public bool IsEnabled { get; set; }

        [Required] 
        public bool IsDeleted { get; set; }

        public List<UserRole> Roles { get; set; }

        [Required]
        public DateTime CreationTime { get; set; }
        
        [Required]
        public DateTime LastUpdateTime { get; set; }
    }
}