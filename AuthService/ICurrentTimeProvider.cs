﻿using System;

namespace AuthService
{
    /// <summary>
    /// Provides current times.
    /// </summary>
    public interface ICurrentTimeProvider
    {
        /// <summary>
        /// Gets current time in local time zone.
        /// </summary>
        /// <returns>Current time for local timezone</returns>
        DateTime GetCurrentTime();
        
        /// <summary>
        /// Gets current time in UTC.
        /// </summary>
        /// <returns>Current time in UTC</returns>
        DateTime GetCurrentUtcTime();
    }
}