﻿using System.Timers;

namespace AuthService
{
    /// <summary>
    /// Represents wrap for System.Timers.Timer
    /// </summary>
    public interface ITimer
    {
        event ElapsedEventHandler Elapsed;
        double Interval { get; set; }
        bool AutoReset { get; set; }
        void Dispose();
        void Start();
        void Stop();
    }
}