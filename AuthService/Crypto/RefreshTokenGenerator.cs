﻿using System;
using System.Security.Cryptography;

namespace AuthService.Crypto
{
    public class RefreshTokenGenerator : IRefreshTokenGenerator
    {
        private const int TokenSizeBytes = 256;
        
        public string GenerateRefreshToken()
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                var tokenData = new byte[TokenSizeBytes];
                rng.GetBytes(tokenData);

                return Convert.ToBase64String(tokenData);
            }
        }
    }
}