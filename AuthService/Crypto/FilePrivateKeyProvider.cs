﻿using System;
using System.IO;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace AuthService.Crypto
{
    // NOTE: So far for testing
    public class FilePrivateKeyProvider : IPrivateKeyProvider
    { 
        private readonly Lazy<string> _rawPrivateKey = new Lazy<string>(LoadKeyFromFile(""));

        public RsaSecurityKey GetPrivateKey()
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.ImportFromEncryptedPem(_rawPrivateKey.Value, "63306330");
            
            return new RsaSecurityKey(rsa);
        }

        private static string LoadKeyFromFile(string path)
        {
            if (String.IsNullOrEmpty(path))
                throw new ArgumentException("Path to key file is null or empty.", nameof(path));
            
            if (!File.Exists(path)) 
                throw new FileLoadException($"File {path} with private key doesnt exists.");

            return File.ReadAllText(path);
        }
    }
}