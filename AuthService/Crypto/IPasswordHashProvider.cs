﻿namespace AuthService.Crypto
{
    /// <summary>
    /// Provides hashing for passwords.
    /// </summary>
    public interface IPasswordHashProvider
    {
        /// <summary>
        /// Create hash for given password.
        /// </summary>
        /// <param name="password">Password to be hashed</param>
        /// <returns>Hash of given password as string</returns>
        string HashPassword(string password);
    }
}