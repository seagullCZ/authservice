﻿namespace AuthService.Crypto
{
    /// <summary>
    /// Generates refresh tokens.
    /// </summary>
    public interface IRefreshTokenGenerator
    {
        /// <summary>
        /// Generate new refresh token and return it as base64 string.
        /// </summary>
        /// <returns>New refresh token as base64 string</returns>
        string GenerateRefreshToken();
    }
}