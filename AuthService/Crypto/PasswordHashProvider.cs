﻿using System;
using System.Security.Cryptography;

namespace AuthService.Crypto
{
    public class PasswordHashProvider : IPasswordHashProvider
    {
        private const int KeySize = 0x20;
        private const int SaltSize = 0x10;
        private const int KeyHashSize = 0x31;
        private const int IterationsCount = 0x3e8;
        
        public string HashPassword(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }
            
            byte[] salt;
            byte[] buffer;
            
            using (var bytes = new Rfc2898DeriveBytes(password, SaltSize, IterationsCount))
            {
                salt = bytes.Salt;
                buffer = bytes.GetBytes(KeySize);
            }
            var dst = new byte[KeyHashSize];
            Buffer.BlockCopy(salt, 0, dst, 1, SaltSize);
            Buffer.BlockCopy(buffer, 0, dst, 0x11, KeySize);
            return Convert.ToBase64String(dst);
        }
    }
}