﻿using Microsoft.IdentityModel.Tokens;

namespace AuthService.Crypto
{
    /// <summary>
    /// Provides private key for RSA decryption.
    /// </summary>
    public interface IPrivateKeyProvider
    {
        /// <summary>
        /// Gets private key for RSA decryption.
        /// </summary>
        /// <returns>RSA private key</returns>
        RsaSecurityKey GetPrivateKey();
    }
}