﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AuthService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace AuthService.Crypto
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        private const string JwtKeyConfigurationKey = "Jwt:Key";
        private const string JwtIssuerConfigurationKey = "Jwt:Issuer";
        private readonly ICurrentTimeProvider _timeProvider;
        private readonly string _key;
        private readonly string _issuer;
        private readonly int _defaultTokenLifespan;

        public JwtTokenGenerator(ICurrentTimeProvider timeProvider, IConfiguration config)
        {
            if (config == null) 
                throw new ArgumentNullException(nameof(config));
            
            _timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
    
            _key = config.GetValue<string>(JwtKeyConfigurationKey);
            if (String.IsNullOrEmpty(_key))
                throw new ArgumentException($"{JwtKeyConfigurationKey} configuration missing or it's empty.");
            
            _issuer = config.GetValue<string>(JwtIssuerConfigurationKey);
            if (String.IsNullOrEmpty(_issuer))
                throw new ArgumentException($"{JwtIssuerConfigurationKey} configuration missing or it's empty.");
            
            _defaultTokenLifespan = config.GetValue("Jwt:TokenLifespanSeconds",120);
        }
        
        public string GenerateJwtToken(User user)
        {
            if (user == null) 
                throw new ArgumentNullException(nameof(user));
            
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key));    
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);
            
            var token = new JwtSecurityToken(_issuer,    
                _issuer,    
                CreateUserClaims(user),    
                expires: _timeProvider.GetCurrentTime().AddSeconds(_defaultTokenLifespan),    
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private static IEnumerable<Claim> CreateUserClaims(User user)
        {
            if (String.IsNullOrWhiteSpace(user.Login))
                throw new ArgumentException("User login is null or whitespace.");
            
            if(user.Roles == null)
                throw new ArgumentException("User roles are null.");
            
            return new List<Claim>
            {
                new ("name",user.Login),
                new ("roles",string.Join(",",user.Roles.Select(r => r.Role.Name))),
                new ("isEnabled",user.IsEnabled.ToString()),
            };
        }
    }
}