﻿using AuthService.Models;

namespace AuthService.Crypto
{
    /// <summary>
    /// Generates JWT tokens.
    /// </summary>
    public interface IJwtTokenGenerator
    {
        /// <summary>
        /// Generate JWT token for given user as base64 string.
        /// </summary>
        /// <param name="user">User for which will be token generated</param>
        /// <returns>JWT token as base64 string</returns>
        string GenerateJwtToken(User user);
    }
}