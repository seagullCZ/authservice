﻿using System;

namespace AuthService
{
    public class CurrentTimeProvider : ICurrentTimeProvider
    {
        public DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }

        public DateTime GetCurrentUtcTime()
        {
            return DateTime.UtcNow;
        }
    }
}