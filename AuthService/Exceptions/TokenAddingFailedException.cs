﻿using System;

namespace AuthService.Exceptions
{
    public class TokenAddingFailedException : Exception
    {
        public TokenAddingFailedException(string message) : base(message)
        {
            
        }
    }
}