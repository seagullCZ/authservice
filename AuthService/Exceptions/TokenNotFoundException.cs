﻿using System;

namespace AuthService.Exceptions
{
    public class TokenNotFoundException : Exception
    {
        public TokenNotFoundException(string message) : base(message)
        {
        }
    }
}