﻿using System;

namespace AuthService.Exceptions
{
    public abstract class UserException : Exception
    {
        public string UserName { get; private set; }

        public UserException(string userName, string message): base(message)
        {
            UserName = userName;
        }
    }
}