﻿namespace AuthService.Exceptions
{
    public class UserAlreadyExistsException : UserException
    {
        public UserAlreadyExistsException(string userName, string message) : base(userName, message)
        {
        }
    }
}