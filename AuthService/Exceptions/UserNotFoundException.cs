﻿using System;

namespace AuthService.Exceptions
{
    public class UserNotFoundException : UserException
    {
        public UserNotFoundException(string userName, string message) : base(userName, message)
        {
        }
    }
}