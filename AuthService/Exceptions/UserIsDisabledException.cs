﻿using System;

namespace AuthService.Exceptions
{
    public class UserIsDisabledException : UserException
    {
        public UserIsDisabledException(string userName, string message) : base(userName, message)
        {
        }
    }
}