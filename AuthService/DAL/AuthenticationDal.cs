﻿using System;
using System.Threading.Tasks;
using AuthService.Crypto;
using AuthService.Exceptions;
using AuthService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AuthService.DAL
{
    public class AuthenticationDal : IAuthenticationDal
    {
        private readonly IDbContextFactory<AuthServiceDbContext> _dbContextFactory;
        private readonly IPasswordHashProvider _passwordHashProvider;
        private readonly ILogger<AuthenticationDal> _logger;
        
        public AuthenticationDal(IDbContextFactory<AuthServiceDbContext> dbContextFactory, IPasswordHashProvider passwordHashProvider, 
            ILogger<AuthenticationDal> logger)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
            _passwordHashProvider = passwordHashProvider ?? throw new ArgumentNullException(nameof(passwordHashProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<User> AuthenticateAsync(string login, string password)
        {
            using (var context = _dbContextFactory.CreateDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(u => 
                    u.Login == login 
                    && u.Password == _passwordHashProvider.HashPassword(password));

                if (user == null || user.IsDeleted)
                    throw new UserNotFoundException(login, $"User {login} not found or password doesnt match.");
                
                if (!user.IsEnabled)
                    throw new UserIsDisabledException(login, 
                        $"Could not authenticate user {login}, because is disabled.");
                
                return user;
            }
        }

        public async Task<User> GetUser(string login, bool includeDisabled = false, bool includeDeleted = false)
        {
            if (login == null)
                throw new ArgumentNullException(nameof(login));
            
            using (var context = _dbContextFactory.CreateDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(u => 
                    u.Login == login);

                if (user == null)
                {
                    var message = $"User with login {login} not found.";
                    _logger.LogDebug(message);
                    throw new UserNotFoundException(login, message);
                }
                
                if (!includeDisabled && !user.IsEnabled)
                {
                    var message = $"User with login {login} is disabled.";
                    _logger.LogDebug(message);
                    throw new UserNotFoundException(login, message);
                }
                
                if (!includeDeleted && user.IsDeleted)
                {
                    var message = $"User with login {login} is deleted.";
                    _logger.LogWarning(message);
                    throw new UserNotFoundException(login, message);
                }

                return user;
            }
        }
        
        public async Task DeleteUserAsync(string login)
        {
            if (login == null)
                throw new ArgumentNullException(nameof(login));
            
            using (var context = _dbContextFactory.CreateDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(u => 
                    u.Login == login
                    && !u.IsDeleted);
                if (user == null)
                {
                    var message = $"Trying to delete unknown user {login}";
                    _logger.LogDebug(message);
                    throw new UserNotFoundException(login, message);
                }

                _logger.LogInformation($"Deleting user {login}");
                user.IsDeleted = true;
                
                await context.SaveChangesAsync();
            }
        }

        public async Task<User> UpdateUserAsync(User userToUpdate)
        {
            if (userToUpdate == null)
                throw new ArgumentNullException(nameof(userToUpdate));
            
            using (var context = _dbContextFactory.CreateDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(u => 
                    u.Login == userToUpdate.Login
                    && !u.IsDeleted);
                if (user == null)
                {
                    var message = $"Deleting unknown user {userToUpdate.Login}";
                    _logger.LogDebug(message);
                    throw new UserNotFoundException(userToUpdate.Login, message);
                }

                user.Password = _passwordHashProvider.HashPassword(userToUpdate.Password);
                user.Roles = userToUpdate.Roles;
                user.IsDeleted = userToUpdate.IsEnabled;
                user.LastUpdateTime = DateTime.UtcNow;

                await context.SaveChangesAsync();
                return user;
            }
        }

        public async Task<User> CreateUserAsync(User newUser)
        {
            if (newUser == null)
                throw new ArgumentNullException(nameof(newUser));

            using (var context = _dbContextFactory.CreateDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(u => u.Login == newUser.Login);
                if (user != null)
                {
                    var message = $"Creating user, but user already exists {newUser.Login}";
                    _logger.LogDebug(message);
                    throw new UserAlreadyExistsException(newUser.Login, message);
                }

                context.Users.Add(newUser);
                await context.SaveChangesAsync();

                return newUser;
            }
        }
    }
}