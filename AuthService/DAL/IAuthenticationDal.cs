﻿using System.Threading.Tasks;
using AuthService.Models;

namespace AuthService.DAL
{
    public interface IAuthenticationDal
    {
        Task<User> AuthenticateAsync(string login, string passwordHash);
        Task<User> GetUser(string login, bool includeDisabled, bool includeDeleted);
        Task<User> CreateUserAsync(User newUser);
        Task<User> UpdateUserAsync(User userToUpdate);
        Task DeleteUserAsync(string login);
    }
}