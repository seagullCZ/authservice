﻿using System.Timers;

namespace AuthService
{
    public class InternalTimer : ITimer
    {
        private readonly Timer _internalTimer;
        
        public double Interval
        {
            get => _internalTimer.Interval;
            set => _internalTimer.Interval = value;
        }

        public bool AutoReset
        {
            get => _internalTimer.AutoReset;
            set => _internalTimer.AutoReset = value;
        }

        public InternalTimer(Timer internalTimer)
        {
            _internalTimer = internalTimer;
        }

        public void Start()
        {
            _internalTimer.Start();
        }

        public void Stop()
        {
            _internalTimer.Stop();
        }

        public event ElapsedEventHandler Elapsed
        {
            add => _internalTimer.Elapsed += value;
            remove => _internalTimer.Elapsed -= value;
        }

        public void Dispose()
        {
            _internalTimer?.Dispose();
        }
    }
}