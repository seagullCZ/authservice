﻿using System;
using AuthService.Crypto;
using AuthService.Exceptions;
using Microsoft.Extensions.Logging;

namespace AuthService.Refresh
{
    public class RefreshTokenManager : IRefreshTokenManager
    {
        private readonly IRefreshTokenInfoStorage _refreshTokenInfoStorage;
        private readonly IRefreshTokenGenerator _refreshTokenGenerator;
        private readonly ICurrentTimeProvider _currentTimeProvider;
        private readonly ILogger<RefreshTokenManager> _logger;
        
        // TODO: Create some reasonable timespan, load default time from settings
        private static readonly TimeSpan DefaultValidTimeSpan = TimeSpan.FromDays(30);

        public RefreshTokenManager(IRefreshTokenInfoStorage refreshTokenInfoStorage, IRefreshTokenGenerator refreshTokenGenerator,
            ICurrentTimeProvider currentTimeProvider, ILogger<RefreshTokenManager> logger)
        {
            _refreshTokenInfoStorage = refreshTokenInfoStorage ?? throw new ArgumentNullException(nameof(refreshTokenInfoStorage));
            _refreshTokenGenerator = refreshTokenGenerator ?? throw new ArgumentNullException(nameof(refreshTokenGenerator));
            _currentTimeProvider = currentTimeProvider ?? throw new ArgumentNullException(nameof(currentTimeProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        
        public string GetOwnerOfToken(string token)
        {
            if (token == null) 
                throw new ArgumentNullException(nameof(token));

            var tokenInfo = _refreshTokenInfoStorage.GetTokenInfo(token);
            var currentTime = _currentTimeProvider.GetCurrentUtcTime();
            if (tokenInfo.IsValid(currentTime)) 
                return tokenInfo.UserLogin;
            
            _logger.LogDebug($"Token for user {tokenInfo.UserLogin} expired, removing it from storage.");
            throw new TokenNotFoundException("Token not found in the storage.");
        }

        public string CreateTokenForOwner(string userLogin)
        {
            if (userLogin == null) 
                throw new ArgumentNullException(nameof(userLogin));

            var newTokenInfo = new RefreshTokenInfo(
                userLogin, _refreshTokenGenerator.GenerateRefreshToken(), 
                DefaultValidTimeSpan, DateTime.UtcNow);
            _refreshTokenInfoStorage.AddTokenInfo(newTokenInfo);
            
            _logger.LogDebug($"Creating refresh token for user {userLogin}");
            
            return newTokenInfo.Token;
        }

        public void RevokeAllTokens()
        {
            _logger.LogInformation("Revoking all refresh tokens in storage tokens of user.");
            _refreshTokenInfoStorage.RemoveAllTokens();
        }

        public void RevokeToken(string token)
        {
            if (token == null) 
                throw new ArgumentNullException(nameof(token));
            
            _logger.LogDebug($"Revoking {token} refresh token.");
            _refreshTokenInfoStorage.RemoveToken(token);
        }

        public void RevokeTokensOfOwner(string userLogin)
        {
            if (userLogin == null) 
                throw new ArgumentNullException(nameof(userLogin));
            
            _logger.LogInformation($"Revoking tokens of user {userLogin}");
            _refreshTokenInfoStorage.RemoveAllOwnerTokens(userLogin);
        }
    }
}