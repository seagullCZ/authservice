﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using AuthService.Exceptions;
using Microsoft.Extensions.Logging;

namespace AuthService.Refresh
{
    public class RefreshTokenInfoInMemStorage : IRefreshTokenInfoStorage, IDisposable
    {
        private readonly ICurrentTimeProvider _currentTimeProvider;
        private readonly ITimer _storageCleanTimer;
        private readonly ILogger<RefreshTokenInfoInMemStorage> _logger;
        protected readonly ConcurrentDictionary<string, RefreshTokenInfo> _storage;

        public RefreshTokenInfoInMemStorage(ICurrentTimeProvider currentTimeProvider, ITimer storageCleanTimer, ILogger<RefreshTokenInfoInMemStorage> logger)
        {
            _currentTimeProvider = currentTimeProvider ?? throw new ArgumentNullException(nameof(currentTimeProvider));
            _storageCleanTimer = storageCleanTimer ?? throw new ArgumentNullException(nameof(storageCleanTimer));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _storage = new ConcurrentDictionary<string, RefreshTokenInfo>();

            _storageCleanTimer.Elapsed += StorageCleanOnElapsed;
            _storageCleanTimer.Interval = TimeSpan.FromHours(1).Milliseconds;
            _storageCleanTimer.AutoReset = true;
            _storageCleanTimer.Start();
        }

        private void StorageCleanOnElapsed(object sender, ElapsedEventArgs e)
        {
            var currentTime = _currentTimeProvider.GetCurrentUtcTime();
            foreach (var invalidToken in _storage.Values.Where(v => !v.IsValid(currentTime)))
            {
                if (_storage.Remove(invalidToken.Token, out var removedToken))
                {
                    _logger.LogDebug($"Token {removedToken.Token} owned by {removedToken.UserLogin} removed due to expiration.");
                }
            }
        }

        public void AddTokenInfo(RefreshTokenInfo token)
        {
            if (token == null) 
                throw new ArgumentNullException(nameof(token));
            
            _logger.LogDebug($"Adding token for owner: {token.UserLogin}");

            if (_storage.TryAdd(token.Token, token))
            {
                _logger.LogDebug($"Token for user {token.UserLogin} successfully added into storage.");
                return;
            }

            const string message = "Token is already in the storage.";
            _logger.LogWarning(message);
            throw new TokenAddingFailedException(message);
        }

        public RefreshTokenInfo GetTokenInfo(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(token));

            if (_storage.TryGetValue(token, out var tokenInfo))
            {
                return tokenInfo;
            }

            const string message = "Token is not present in the storage.";
            _logger.LogWarning(message);
            throw new TokenNotFoundException(message);
        }

        public void RemoveToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(token));

            if (_storage.TryRemove(token, out var tokenInfo))
            {
                _logger.LogInformation($"Removing token {tokenInfo.Token} of owner {tokenInfo.UserLogin}");
                return;
            }

            const string message = "Token is not present in the storage.";
            _logger.LogWarning(message);
            throw new TokenNotFoundException(message);
        }

        public void RemoveAllOwnerTokens(string userLogin)
        {
            if (string.IsNullOrWhiteSpace(userLogin))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userLogin));
            
            var revokedTokensCount = 0;
            foreach (var refreshTokenInfo in _storage.Values.Where(t => t.UserLogin == userLogin))
            {
                if (_storage.TryRemove(refreshTokenInfo.Token, out var removedToken))
                {
                    revokedTokensCount++;
                    _logger.LogDebug($"Removing token {removedToken.Token} owned by {removedToken.UserLogin}");
                }
            }
            
            _logger.LogInformation($"{revokedTokensCount} tokens of {userLogin} user revoked");
        }

        public void RemoveAllTokens()
        {
            int removedTokens = 0;
            foreach (var token in _storage.Keys)
            {
                if(_storage.TryRemove(token, out var removedToken))
                {
                    removedTokens++;
                    _logger.LogDebug($"Removing token {removedToken.Token} owned by {removedToken.UserLogin}");
                }
            }
            
            _logger.LogInformation($"Revoke all tokens called, revoked (removed) {removedTokens} refresh tokens.");
        }

        public void Dispose()
        {
            _storageCleanTimer?.Dispose();
        }
    }
}