﻿using System;

namespace AuthService.Refresh
{
    public class RefreshTokenInfo
    {
        public string Token { get; }
        public string UserLogin { get; }
        
        private readonly TimeSpan _validFor;
        private readonly DateTime _timestamp;

        public RefreshTokenInfo(string userLogin, string token, TimeSpan validFor, DateTime timestamp)
        {
            UserLogin = userLogin ?? throw new ArgumentNullException(nameof(userLogin));
            Token = token ?? throw new ArgumentNullException(nameof(token));
            
            _validFor = validFor;
            _timestamp = timestamp;
        }
        
        public bool IsValid(DateTime currentTime)
        {
            return  currentTime < _timestamp + _validFor;
        }

        public override int GetHashCode()
        {
            return (Token, User: UserLogin).GetHashCode();
        }
    }
}