﻿namespace AuthService.Refresh
{
    /// <summary>
    /// Manages refresh tokens.
    /// </summary>
    public interface IRefreshTokenManager
    {
        /// <summary>
        /// Gets owner (user) for given refresh token.
        /// </summary>
        /// <param name="token">Token for which will be user searched</param>
        /// <returns>Owner username</returns>
        string GetOwnerOfToken(string token);
        
        /// <summary>
        /// Creates refresh token for given user.
        /// </summary>
        /// <param name="userLogin">User for which will be new refresh token created</param>
        /// <returns>New refresh token for given user</returns>
        string CreateTokenForOwner(string userLogin);
        
        /// <summary>
        /// Revokes all stored refresh tokens.
        /// </summary>
        void RevokeAllTokens();
        
        /// <summary>
        /// Revoke specific refresh token.
        /// </summary>
        /// <param name="token">Token which should be revoked</param>
        void RevokeToken(string token);
        
        /// <summary>
        /// Revoke all refresh tokens of given user.
        /// </summary>
        /// <param name="userLogin">User for which will be all tokens revoked</param>
        void RevokeTokensOfOwner(string userLogin);
    }
}