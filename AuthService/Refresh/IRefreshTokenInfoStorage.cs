﻿namespace AuthService.Refresh
{
    /// <summary>
    /// Stores refresh tokens and offers management over them.
    /// </summary>
    public interface IRefreshTokenInfoStorage
    {
        /// <summary>
        /// Adds new token into storage.
        /// </summary>
        /// <param name="token">New token specified by token info</param>
        void AddTokenInfo(RefreshTokenInfo token);
        
        /// <summary>
        /// Gets token info for given token.
        /// </summary>
        /// <param name="token">Token for which will be token info searched</param>
        /// <returns>Token info for given token</returns>
        RefreshTokenInfo GetTokenInfo(string token);
        
        /// <summary>
        /// Removes token with token info from storage.
        /// </summary>
        /// <param name="token">Token which will be removed</param>
        void RemoveToken(string token);
        
        /// <summary>
        /// Removes all tokens for given owner from storage.
        /// </summary>
        /// <param name="userLogin">User for which will be all tokens removed</param>
        void RemoveAllOwnerTokens(string userLogin);
        
        /// <summary>
        /// Remove all tokens from storage.
        /// </summary>
        void RemoveAllTokens();
    }
}