﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuthService.Crypto;
using AuthService.DAL;
using AuthService.Exceptions;
using AuthService.Models;
using AuthService.Refresh;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace AuthService.Services
{
    public class AuthenticationService : AuthService.AuthServiceBase
    {
        private readonly IAuthenticationDal _authDal;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IRefreshTokenManager _refreshTokenManager;
        private readonly ILogger<AuthenticationService> _logger;

        public AuthenticationService(IAuthenticationDal authDal, IJwtTokenGenerator jwtTokenGenerator,
        IRefreshTokenManager refreshTokenManager, ILogger<AuthenticationService> logger)
        {
            _authDal = authDal ?? throw new ArgumentNullException(nameof(authDal));
            _jwtTokenGenerator = jwtTokenGenerator ?? throw new ArgumentNullException(nameof(jwtTokenGenerator));
            _refreshTokenManager = refreshTokenManager ?? throw new ArgumentNullException(nameof(refreshTokenManager));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task<AuthenticationReply> Authenticate(AuthenticationRequest request,
            ServerCallContext context)
        {
            _logger.LogDebug("Authenticate called");

            var login = request.Credentials.Login;
            var password = request.Credentials.Password;

            var user = await _authDal.AuthenticateAsync(login, password);

            if (user == null)
            {
                // unknown user - return fail
                return CreateAuthenticateReply(String.Empty, ReplyState.Error,
                    $"Authentication failed for user {login}.");
            }

            // create and return token
            var token = _jwtTokenGenerator.GenerateJwtToken(user);
            return CreateAuthenticateReply(token, ReplyState.Success, String.Empty);
        }

        private static AuthenticationReply CreateAuthenticateReply(string token, ReplyState state, string message)
        {
            var authReply = new AuthenticationReply
            {
                Status = new ReplyStatus
                {
                    State = state,
                    Message = message
                },
                Token = token
            };

            return authReply;
        }

        public override async Task<RefreshTokenReply> RefreshToken(RefreshTokenRequest request,
            ServerCallContext context)
        {
            _logger.LogDebug("RefreshToken called");
            var token = request.RefreshToken;

            if (String.IsNullOrWhiteSpace(token))
            {
                _logger.LogWarning($"Invalid refresh token received: {token}");
                return CreateRefreshTokenReply(String.Empty, ReplyState.Error, "Invalid token");
            }

            try
            {
                var refreshTokenOwner = _refreshTokenManager.GetOwnerOfToken(token);
                var owner = await _authDal.GetUser(refreshTokenOwner, false, false);
                var jwtToken = _jwtTokenGenerator.GenerateJwtToken(owner);
                return CreateRefreshTokenReply(jwtToken, ReplyState.Success, String.Empty);
            }
            catch (TokenNotFoundException ex)
            {
                var message = $"Owner of given token not found. Exception {ex}";
                _logger.LogDebug(message);
                return CreateRefreshTokenReply(String.Empty, ReplyState.Error, message);
            }
            catch (Exception ex)
            {
                var message = $"Error occured during refreshing token for user. Exception {ex}";
                _logger.LogError(message);
                return CreateRefreshTokenReply(String.Empty, ReplyState.Error, message);
            }
        }

        private static RefreshTokenReply CreateRefreshTokenReply(string token, ReplyState state, string message)
        {
            var reply = new RefreshTokenReply
            {
                Status = new ReplyStatus
                {
                    State = state,
                    Message = message
                },
                Token = token
            };

            return reply;
        }

        public override async Task<CreateUserReply> CreateUser(CreateUserRequest request, ServerCallContext context)
        {
            _logger.LogDebug("CreateUser called");

            try
            {
                var newUser = GetUserFromUserInfo(request.NewUser);
                _ = await _authDal.CreateUserAsync(newUser);
                return CreateCreateUserReply(ReplyState.Success, String.Empty);
            }
            catch (ArgumentException ex)
            {
                _logger.LogWarning($"CreateUserRequest corrupted, missing some information. Exception: {ex}");
                return CreateCreateUserReply(ReplyState.Error, "Create user request corrupted.");
            }
            catch (UserAlreadyExistsException ex)
            {
                _logger.LogDebug($"Exception occured during creating user: {ex}");
                return CreateCreateUserReply(ReplyState.Error, "User already exists.");
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"Exception occured during creating user: {ex}");
                return CreateCreateUserReply(ReplyState.Error, "Server error ");
            }
        }

        private static User GetUserFromUserInfo(UserInfo userInfo)
        {
            if (userInfo == null)
                throw new ArgumentException("New user is missing in request.");
            
            if(userInfo.Credentials == null)
                throw new ArgumentException("New user credentials are missing in request.");
            
            var user = new User
            {
                Login = userInfo.Credentials.Login,
                Password = userInfo.Credentials.Password,
                IsDeleted = false,
                IsEnabled = userInfo.IsEnabled,
            };
            user.Roles = GetUserRolesFromUserInfo(userInfo, user);
            
            return user;
        }

        private static List<UserRole> GetUserRolesFromUserInfo(UserInfo userInfo, User user)
        {
            var userRoles = new List<UserRole>();

            if (userInfo.Roles == null)
                return userRoles;

            foreach (var role in userInfo.Roles)
            {
                userRoles.Add(new UserRole
                {
                    Role = new Models.Role{Name = role},
                    User = user
                });
            }
            
            return userRoles;
        }

        private static CreateUserReply CreateCreateUserReply(ReplyState state, string message)
        {
            return new CreateUserReply
            {
                Status = new ReplyStatus()
                {
                    Message = message,
                    State = state
                }
            };
        }

        public override async Task<DeleteUserReply> DeleteUser(DeleteUserRequest request, ServerCallContext context)
        {
            _logger.LogDebug("DeleteUser called");
            
            var userToDeleteLogin = request.UserLogin;
            if (String.IsNullOrWhiteSpace(userToDeleteLogin))
            {
                _logger.LogWarning($"User to delete login is invalid: {userToDeleteLogin}");
                return CreateDeleteUserReply(ReplyState.Error, "Invalid user login");
            }

            try
            {
                await _authDal.DeleteUserAsync(userToDeleteLogin);
                return  CreateDeleteUserReply(ReplyState.Success, String.Empty);
            }
            catch (UserNotFoundException ex)
            {
                var message = $"Could not delete user {userToDeleteLogin}. User not found.";
                _logger.LogDebug(ex, message);
                return CreateDeleteUserReply(ReplyState.Error,message);
            }
            catch (Exception ex)
            {
                var message = $"Could not delete {userToDeleteLogin} user, server error";
                _logger.LogError(ex,message);
                return CreateDeleteUserReply(ReplyState.Error,message);
            }
        }

        private static DeleteUserReply CreateDeleteUserReply(ReplyState state, string message)
        {
            return new DeleteUserReply
            {
                Status = new ReplyStatus
                {
                    State = state,
                    Message = message
                }
            };
        }

        public override async Task<UpdateUserReply> UpdateUser(UpdateUserRequest request, ServerCallContext context)
        {
            _logger.LogDebug("UpdateUser called");

            try
            {
                var userToUpdate = GetUserFromUserInfo(request.UpdatedUser);
                _ = await _authDal.UpdateUserAsync(userToUpdate);
                return CreateUpdateUserReply(ReplyState.Success, String.Empty);
            }
            catch (ArgumentException ex)
            {
                _logger.LogWarning($"UpdateUserRequest corrupted, missing some information. Exception: {ex}");
                return CreateUpdateUserReply(ReplyState.Error, "Update user request corrupted.");
            }
            catch (UserNotFoundException ex)
            {
                _logger.LogWarning($"Updating user, but user not found. {ex}");
                return CreateUpdateUserReply(ReplyState.Error, "User doesn't exist found.");
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"Exception occured during creating user: {ex}");
                return CreateUpdateUserReply(ReplyState.Error, "Server error ");
            }
        }

        private static UpdateUserReply CreateUpdateUserReply(ReplyState state, string message)
        {
            return new UpdateUserReply
            {
                Status = new ReplyStatus()
                {
                    Message = message,
                    State = state
                }
            };
        }

        public override Task<RevokeRefreshTokenReply> RevokeRefreshToken(RevokeRefreshTokenRequest request, ServerCallContext context)
        {
            return base.RevokeRefreshToken(request, context);
        }

        public override Task<RevokeAllRefreshTokensReply> RevokeAllRefreshTokens(RevokeAllRefreshTokensRequest request, ServerCallContext context)
        {
            return base.RevokeAllRefreshTokens(request, context);
        }

        public override Task<RevokeAllUserRefreshTokensReply> RevokeAllUserRefreshTokens(RevokeAllUserRefreshTokensRequest request, ServerCallContext context)
        {
            return base.RevokeAllUserRefreshTokens(request, context);
        }
    }
}